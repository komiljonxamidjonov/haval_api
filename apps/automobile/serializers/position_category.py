# Django REST framework
from rest_framework import serializers

# Projects
from automobile.models import PositionCategory


class PositionCategoryModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = PositionCategory
        fields = "__all__"


class ComparePositionSerializer(serializers.ModelSerializer):
    class Meta:
        model = PositionCategory
        fields = [
            "lockingRearWheelDifferential",
            "automaticAutoHold",
            "childSeatLock",
            "led_headlight",
            "full_weight",
            "transmission_box",
            "seats_count",
            "max_speed",
        ]