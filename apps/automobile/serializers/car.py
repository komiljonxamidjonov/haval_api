# Django REST framework
from rest_framework import serializers

# Projects
from automobile.models import Car


class CarModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = "__all__"



