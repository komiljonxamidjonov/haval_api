# Django REST framework
from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter, OrderingFilter

# Projects
from automobile.models import PositionCategory
from automobile.serializers import PositionCategoryModelSerializer


class PositionCategoryViewSet(viewsets.ModelViewSet):
    queryset = PositionCategory.objects.order_by('id')
    serializer_class = PositionCategoryModelSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter)

    filterset_fields = "__all__"
    search_field = "__all__"

