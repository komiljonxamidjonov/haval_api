from django.db import models
from shared.django.model import BaseModel


class Cridet(BaseModel):
    CREDIT = 1
    LEASING = 2
    CHOICE = (
        (CREDIT, 'Credit'),
        (LEASING, 'Leasing'),
    )

    # Relations
    car = models.ForeignKey('automobile.Car', on_delete=models.CASCADE)
    model = models.ForeignKey('automobile.PositionCategory', on_delete=models.CASCADE)

    # Fields
    month = models.IntegerField(default=12)
    status = models.IntegerField(choices=CHOICE)

    def __str__(self):
        return str(self.car.title)

    @property
    def model_name(self):
        return self.model.name
