from calculator.serializers.cridet import CridetModelSerializer
from calculator.serializers.payment import PaymentModelSerializer
from calculator.serializers.calculate import CalculateSerializer
from calculator.serializers.calculate_handbook import CreditsCalcSerializer, LeasingCalcSerializer, \
    PositionCreditCalcModelsSerializer, PositionLeasingCalcSerializer, CarCreditModelsSerializer, \
    CarLeasingModelsSerializer
