# Django REST framework
from abc import ABC

from rest_framework import serializers

# Projects
from calculator.models import Payment, Cridet
from automobile.models import PositionCategory, Car


class PaymentModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = "__all__"
        # depth = 1
