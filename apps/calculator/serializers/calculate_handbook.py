# Django REST framework
from rest_framework import serializers

# Projects
from calculator.models import Cridet
from automobile.models import PositionCategory, Car
from calculator.serializers import PaymentModelSerializer


class CreditsCalcSerializer(serializers.ModelSerializer):
    payment = PaymentModelSerializer(source='payment_set', many=True, read_only=True)

    class Meta:
        model = Cridet
        fields = [
            'month',
            'payment'
        ]


class LeasingCalcSerializer(serializers.ModelSerializer):
    payment = PaymentModelSerializer(source='payment_set', many=True, read_only=True)

    class Meta:
        model = Cridet
        fields = [
            'month',
            'payment'
        ]


class PositionCreditCalcModelsSerializer(serializers.ModelSerializer):
    credits = CreditsCalcSerializer(source='cridet_set', many=True, read_only=True)

    class Meta:
        model = PositionCategory
        fields = [
            'id',
            'name',
            'credits'
        ]


class PositionLeasingCalcSerializer(serializers.ModelSerializer):
    leasing = LeasingCalcSerializer(source='cridet_set', many=True, read_only=True)

    class Meta:
        model = PositionCategory
        fields = [
            'id',
            'name',
            'leasing'
        ]
        # depth = 1


class CarCreditModelsSerializer(serializers.ModelSerializer):
    models = PositionCreditCalcModelsSerializer(source='positioncategory_set', many=True, read_only=True)

    class Meta:
        model = Car
        fields = [
            'id',
            'title',
            'models'
        ]


class CarLeasingModelsSerializer(serializers.ModelSerializer):
    models = PositionLeasingCalcSerializer(source='positioncategory_set', many=True, read_only=True)

    class Meta:
        model = Car
        fields = [
            'id',
            'title',
            'models'
        ]

