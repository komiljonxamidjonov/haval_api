# Django REST framework
from rest_framework import serializers

# Projects
from calculator.models import Cridet


class CridetModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cridet
        fields = "__all__"
