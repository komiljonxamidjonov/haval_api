# Django REST framework

from rest_framework import serializers

# Projects
from calculator.serializers.calculate_handbook import CarCreditModelsSerializer, CarLeasingModelsSerializer


class CalculateSerializer(serializers.Serializer):
    credit = CarCreditModelsSerializer(many=True, read_only=True)
    leasing = CarLeasingModelsSerializer(many=True, read_only=True)
