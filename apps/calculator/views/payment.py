# Django REST framework
from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend

# Projects
from calculator.models import Payment, Cridet
from calculator.serializers import PaymentModelSerializer, CalculateSerializer


class PaymentViewSet(viewsets.ModelViewSet):
    queryset = Payment.objects.order_by('id')
    serializer_class = PaymentModelSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['credit']


