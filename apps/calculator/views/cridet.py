# Django REST framework
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

# Projects
from calculator.models import Cridet, Payment
from calculator.serializers import CridetModelSerializer


class CridetViewSet(viewsets.ModelViewSet):
    queryset = Cridet.objects.order_by('id')
    serializer_class = CridetModelSerializer

