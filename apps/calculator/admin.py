from django.contrib import admin
from django.apps import apps
from calculator.models import Payment, Cridet


@admin.register(Cridet)
class CridetAdmin(admin.ModelAdmin):
    list_display = ['id']


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    list_display = ['id']


models = apps.get_models()
for model in models:
    try:
        admin.site.register(model)
    except admin.sites.AlreadyRegistered:
        pass
